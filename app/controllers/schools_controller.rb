class SchoolsController < ApplicationController
  before_action :set_school, only: [:show, :edit, :update, :destroy]

  def index
    @schools = School.all
  end

  def show
  end

  def new
    @school = School.new
  end

  def edit
  end

  def create
    @school = School.new(school_params)

    respond_to do |format|
      if @school.save
        flash[:success] = 'Escola criada com sucesso.'
        format.html { redirect_to @school }
        format.json { render :show, status: :created, location: @school }
      else
        flash[:danger] = 'Erro ai criar escola'
        format.html { render :new }
        format.json { render json: @school.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @school.update(school_params)
        flash[:success] = 'Escola atualizada com sucesso'
        format.html { redirect_to @school }
        format.json { render :show, status: :ok, location: @school }
      else
        flash[:danger] = 'Ocorreram erros ao atualizar a escola'
        format.html { render :edit }
        format.json { render json: @school.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @school.destroy
    respond_to do |format|
      flash[:success] = 'Escola deletado com sucesso'
      format.html { redirect_to schools_url }
      format.json { head :no_content }
    end
  end

  private

    def set_school
      @school = School.find(params[:id])
    end

    def school_params
      params.require(:school).permit(:name, :email, :pitch, :subdomain)
    end
end
