class CoursesController < ApplicationController
  before_action :set_course, only: [:show, :edit, :update, :destroy]

  def index
    @courses = Course.all
  end

  def show
  end

  def new
    @course = Course.new
  end

  def edit
  end

  def create
    @course = Course.new(course_params)

    respond_to do |format|
      if @course.save
        flash[:success] = 'Curso criado com sucesso.'
        format.html { redirect_to @course }
        format.json { render :show, status: :created, location: @course }
      else
        flash[:danger] = 'Erro ao criar curso'
        format.html { render :new }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @course.update(course_params)
        flash[:success] = 'Curso atualizado com sucesso.'
        format.html { redirect_to @course}
        format.json { render :show, status: :ok, location: @course }
      else
        flash[:success] = 'Erro au atualizar curso.'
        format.html { render :edit }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @course.destroy
    respond_to do |format|
      flash[:success] = 'Curso deletado com sucesso.'
      format.html { redirect_to courses_url }
      format.json { head :no_content }
    end
  end

  private
    def set_course
      @course = Course.find(params[:id])
    end

    def course_params
      params.require(:course).permit(:title, :description, :content, :duration, :price)
    end
end
