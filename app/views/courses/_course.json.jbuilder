json.extract! course, :id, :title, :description, :content, :duration, :price, :created_at, :updated_at
json.url course_url(course, format: :json)
