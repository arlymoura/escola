class CreateCourses < ActiveRecord::Migration[5.0]
  def change
    create_table :courses do |t|
      t.string :title
      t.string :description
      t.string :content
      t.integer :duration
      t.decimal :price

      t.timestamps
    end
  end
end
